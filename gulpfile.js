var gulp = require('gulp');
var del = require('del');
var express = require('express');

gulp.task('build', ['clean'], function(){

	return gulp.src('src/**/*.*')
	.pipe(gulp.dest('public'))
})

gulp.task('clean', function(){
	del(['public/**/*.*']);
	console.log('cleaned out build folder');
})

gulp.task('serve', ['watch'], function(){
	var app = express();
	app.use(express.static('public'));
	app.listen(4000, '0.0.0.0');
})

gulp.task('watch', function(){
	gulp.watch('src/**/*.*', ['build'])
})